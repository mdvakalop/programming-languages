(*function taken from softlab.ntua.gr/pl1/2014a/Exercises/agonas.sml*)
fun  first (x,y)  = x
fun second (x,y)  = y


(*this function creats tuple-int-list *)
fun Y_built (n, [],    [])  = []
  | Y_built (n, [],     l)  = l
  | Y_built (0, inList, l)  = l
  | Y_built (n, inList, l)  =
    let
	val height = hd inList   (*this is the last height from the file*)
	val (a,b) = (n, height)
    in
	Y_built (n-1, tl inList, (a,b)::l)
    end



fun Y_creator (F) =
  let
      val inputList = second (F)  (*inputList has the nums reversed*)
      val N = first (F)
  in
      Y_built (N , inputList, [])
  end




(*here i give Y, it gives me right result) *)

fun reverse (xs) =  (*function taken from lecture-6.pdf of pl1*)
  let
      fun rev (nil, z)   = z
	| rev (y::ys, z) = rev (ys, y::z)
  in
      rev (xs, nil)
  end


fun smaller (a,b) = if a<b then true else false 
fun bigger (a,b) = if a>b then true else false      
					      

fun push_back    ( f, nil , nil )   = []
  | push_back    ( f, nil ,  x  )   = x
  | push_back    ( f, y   , nil )   = []
  | push_back    ( f, y   ,  x  )   =
    if f(second(hd y), second(hd x)) then
	push_back (f, tl y, (hd y)::x)
    else push_back (f, tl y, x)
			     
(*borousa na th grapsw xwris p.m. kai na evaza kai orelse???
++ borousa na glytwsw to pwerasma ths f pantou????? *)		   


(*takes Y as input*)		     
fun L_creator   ([])    =   []
  | L_creator   (Yi)    =
 (*| L_creator   (i:int, hght:int) :: []) =  
  | L_creator   (i:int, hght:int) :: Ys  =*) 
    let
	val L  = hd Yi :: []    (*hd Yi is this tuple: (i=0,height0)*)
			      (*val L = (i,hght)::[]*)
    in
	reverse(push_back   (smaller, tl Yi, L))
    end


(*takes reverse(Y) as input and returns reversed R*)		     
fun R_creator   ([])    =   []
  | R_creator   (Yi)    =
    let
	val R  = hd Yi :: []    (*hd Yi is this tuple: (i=0,height0)*)
			      (*val L = (i,hght)::[]*)
    in
	(push_back   (bigger, tl Yi, R))
    end


fun loop1 ([],        (i,hi), dist)  = ([] , dist)
   |loop1 (R, (i,hi), dist)  =
	 if second(hd R) >= hi then
	     if (first(hd R)-i) > dist then loop1 (tl R, (i,hi), first(hd R)-i)
	     else loop1 (tl R, (i,hi), dist)
	 else (R, dist)
			
fun loop_j ([],        (i,hi)) = ([],~1)
  | loop_j (R, (i,hi)) =
    if second(hd R) >= hi then loop1 (tl R, (i,hi), first(hd R)-i)
    else (R, 0)
(*loop_j (R, first(hd L)) returns ("R list which remains", "max dist for i")*)


	     
fun loop2 ([], [], max_dist)     = max_dist
  | loop2 ([], Lrem, max_dist)   = max_dist
  | loop2 (Rrem, [], max_dist)   = max_dist  
  | loop2 (Rrem, Lrem, max_dist) =   (*Lrem = tl L*)
    let
	val dist_i = second ( loop_j (Rrem,  hd Lrem) )
	val R'rem = first ( loop_j (Rrem,  hd Lrem) )
    in
	if dist_i > max_dist then loop2 (R'rem, tl Lrem, dist_i)
	else loop2 (R'rem, tl Lrem, max_dist)
    end
	
fun loop_i (R, L)=
   let
     val Rr = first(loop_j (R, hd L))
     val max_d = second(loop_j (R, hd L))
   in
       loop2 (Rr, (tl L), max_d)
   end

	
fun now ([])   = ~1
  | now (Y)    = loop_i (R_creator (reverse(Y)), L_creator (Y))




fun skitrip file =
  let
    fun next_int input =
	    Option.valOf (TextIO.scanStream (Int.scan StringCvt.DEC) input)
    val stream = TextIO.openIn file
    val n = next_int stream
    fun scanFile 0 acc = acc (* acc is my input list reversed*)
      | scanFile i acc =
            let
                val a = next_int stream
            in
                scanFile (i - 1) (a :: acc)
            end
  in
     now(Y_creator(n, scanFile n []))
  end
      (*returns s tuple (num of elements  ,  list of ints == input)*)
      


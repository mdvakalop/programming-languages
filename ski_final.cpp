#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

vector <int> create_R (vector<int> Yi, int N) {
	int max, i;
	vector<int> R;
	max = Yi[N-1];
	R.push_back(N-1);
	for (i=N-1; i>=0; i--) {
		if (Yi[i] > max) {
			max = Yi[i];
			R.push_back(i);
		}
	}
	return R;
}

vector <int> create_L (vector<int> Yi, int N){
	int min, i;
	vector<int> L;
	min = Yi[0];
	L.push_back(0);
	for (i=1; i<N; i++) {
		if (Yi[i] < min) {
			min = Yi[i];
			L.push_back(i);
		}
	}
	return L;
}


int main(int argc, char **argv) {
	int N, i, height;// dist;
	int max_dist=0;
	vector <int> Y, L, R;
	ifstream myfile;
	
	myfile.open(argv[1]);
	myfile >> N;
	
	for (i=0; i<N; i++) {       //���������� ��� ������ �
		myfile >> height;
		Y.push_back(height);
	}

	L = create_L (Y,N);
	R = create_R (Y,N);
	
	//vector<int> *ptr;
	//ptr = &L;             //�������� ��� L(0): ptr->at(0);  
	int  dist = 0;
	int j = R.size()-1;
	for (i=0; i<(int)L.size(); i++) {	
		while (j>=0 && (Y[L[i]] <= Y[R[j]])) {
			if (dist < (R[j]-L[i])) dist = R[j]-L[i];
			j--;
		}
		if (max_dist < dist) max_dist = dist;
	}
	
	cout << max_dist << endl;
	myfile.close();
	
	return 0;
}



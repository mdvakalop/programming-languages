(*function taken from lecture-6.pdf of pl1*)
fun reverse (xs) =  
  let
      fun rev (nil, z)   = z
	| rev (y::ys, z) = rev (ys, y::z)
  in
      rev (xs, nil)
  end




(*this fun returns line_list without "\n"*)
fun char_list ([]) = []
  | char_list (l)  = reverse(tl (reverse(l)))


(*this fun returns (array2, iStart, jStart). take find_start for 1st time with iSt=jSt=0*)
fun find_start (array, i, j) =
  if  ((Array2.nCols array)-1 < j) orelse ((Array2.nRows array)-1 < i)
  then (array, ~1,~1)
  else
      if (Char.compare(Array2.sub(array,i,j), #"S") = EQUAL) then (array, i, j)
      else
	  if ((Array2.nCols array)-1 = j) then find_start (array, i+1, 0)
	  else find_start (array, i, j+1)

fun find_end (array, i, j) =
  if  ((Array2.nCols array)-1 < j) orelse ((Array2.nRows array)-1 < i)
  then (array, ~1,~1)
  else
      if (Char.compare(Array2.sub(array,i,j), #"E") = EQUAL) then (array, i, j)
      else
	  if ((Array2.nCols array)-1 = j) then find_end (array, i+1, 0)
	  else find_end (array, i, j+1)

fun find_i (array, i, j) = i
fun find_j (array, i, j) = j	  


				 

fun space file =
  let
      val stream = TextIO.openIn file
      fun next_line input = (TextIO.inputLine input)
  in
      let
	  fun built_list (l) =
	    if (TextIO.endOfStream stream) then l
	    else
		let
		    val a = next_line stream
		    val b = explode (Option.valOf(a))
		    val s = char_list (b); 
		in
		    built_list (s::l)
		end
      in
	  Array2.fromList (reverse(built_list ([])))
       end
  end
     



fun getCost  (cost,pizza,i,j)  =  cost
fun getPizza (cost,pizza,i,j)  =  pizza				  
fun getI     (cost,pizza,i,j)  =  i
fun getJ     (cost,pizza,i,j)  =  j


(*sort will bring us to the top the element with the less cost.
this fun is based on merge from courses.softlab.ntua.gr/pl1/2016a/Slides/lecture-04.pdf *)	   

fun halve []  = ([], [])
  | halve [a] = ([a], [])
  | halve (a::b::cs)  =
    let
	val (x,y) = halve cs
    in
	(a::x, b::y)
    end
	
fun merge ([], ys)        = ys
  | merge (xs, [])        = xs
  | merge (x::xs, y::ys)  =
    if getCost(x) < getCost(y) then x :: merge (xs, y::ys)
    else y :: merge (x::xs, ys)
		    

fun  sort_Q ([])  = []
   | sort_Q ([a]) = [a]
   | sort_Q (l)   =
     let
	 val (x,y) = halve l
     in
	 merge (sort_Q (x), sort_Q (y))
     end


				      

				      
(*1:left, 2:right, 3:up, 4:down*)				      
fun addI (1) = 0
  | addI (2) = 0
  | addI (3) = ~1
  | addI (4) = 1
  | addI (n) = 0 

fun addJ (1) = ~1
  | addJ (2) = 1
  | addJ (3) = 0
  | addJ (4) = 0
  | addJ (n) = 0


fun not_X (node, inpArray, move) =
  let
      val i0 = getI(node) + addI(move)
      val j0 = getJ(node) + addJ(move)
  in
      if (Char.compare (Array2.sub(inpArray, i0, j0), #"X") = EQUAL) then false
      else true
  end

      

(*returns a node with the new-left-state*)
fun nextL (cost, pizza, i, j) = (*takes node as variable*)
  if (pizza) then (cost+2, pizza, i, j-1)
  else (cost+1, pizza, i, j-1)

(*returns a node with the new-right-state*)
fun nextR (cost, pizza, i, j) = 
  if (pizza) then (cost+2, pizza, i, j+1)
  else (cost+1, pizza, i, j+1)

(*returns a node with the new-up-state*)
fun nextU (cost, pizza, i, j) = 
  if (pizza) then (cost+2, pizza, i-1, j)
  else (cost+1, pizza, i-1, j)

(*returns a node with the new-down-state*)
fun nextD (cost, pizza, i, j) = 
  if (pizza) then (cost+2, pizza, i+1, j)
  else (cost+1, pizza, i+1, j)

(*returns a node with the new-wormhole-state*)
fun nextW (cost, pizza, i, j) =
  (cost+1, Bool.not(pizza), i, j)
	   
	      

fun get_move (i, j, move, pizza) = move
fun get_prev_i (i,j,move, pizza) = i
fun get_prev_j (i,j,move, pizza) = j
fun get_prev_pizza (i,j,move,pizza) = pizza
				
      
fun is_in_prev (next_node, Q_top, Move, prev_pizza, prev_not_pizza) = (*Move is a char *)
  let
      val iNext = getI(next_node)
      val jNext = getJ(next_node)
      val iQ_top = getI (Q_top)
      val jQ_top = getJ (Q_top)
      val pizzaQ_top = getPizza (Q_top)				
  in
      if (getPizza(next_node)) then (*if carrys pizza update prev_pizza array*)
	  if Char.compare(get_move(Array2.sub (prev_pizza, iNext, jNext)), #"Z") = EQUAL then (*if it isn;t in prev*)
	      let
		  val _ = Array2.update (prev_pizza, iNext, jNext, (iQ_top, jQ_top, Move, pizzaQ_top)) (*update*)
	      in
		  (false, prev_pizza, prev_not_pizza)
	      end
	  else (true,prev_pizza, prev_not_pizza)
      else
	  if Char.compare(get_move(Array2.sub (prev_not_pizza, iNext, jNext)), #"Z") = EQUAL then (*if it isn;t in prev*)
	      let
		  val _ = Array2.update (prev_not_pizza, iNext, jNext, (iQ_top, jQ_top, Move, pizzaQ_top)) (*update*)
	      in
		  (false,prev_pizza,prev_not_pizza)
	      end
	 else (true,prev_pizza, prev_not_pizza)
 end
      
	  
fun get_bool (a,b,c) = a			  


			   
fun l_move (Q, Q_top, prev_pizza, prev_not_pizza) =
     if (getJ(Q_top)-1 >= 0) andalso not_X(Q_top, (space "data.txt"), 1)
     then
	 if get_bool(is_in_prev(nextL(Q_top), Q_top, #"L", prev_pizza, prev_not_pizza)) (*if it is in prev*)
	 then (Q, prev_pizza, prev_not_pizza)
	 else (nextL(Q_top)::Q, prev_pizza, prev_not_pizza)  (*if its not push it in Q*)
     else  (Q, prev_pizza, prev_not_pizza)   
      

fun r_move (Q, Q_top, prev_pizza, prev_not_pizza) =
     if (getJ(Q_top)+1 >= 0) andalso not_X(Q_top, (space "data.txt"), 2)
     then
	 if get_bool(is_in_prev(nextR(Q_top), Q_top, #"R", prev_pizza, prev_not_pizza)) 
	 then (Q, prev_pizza, prev_not_pizza) 
	 else (nextR(Q_top)::Q, prev_pizza, prev_not_pizza)  
     else (Q, prev_pizza, prev_not_pizza)    
      

fun u_move (Q, Q_top, prev_pizza, prev_not_pizza) =
     if (getI(Q_top)-1 >= 0) andalso not_X(Q_top, (space "data.txt"), 3)
     then
	 if get_bool(is_in_prev(nextU(Q_top), Q_top, #"U", prev_pizza, prev_not_pizza))
	 then (Q, prev_pizza, prev_not_pizza) 
	 else (nextU(Q_top)::Q, prev_pizza, prev_not_pizza)  
     else  (Q, prev_pizza, prev_not_pizza)    
      	       

fun d_move (Q, Q_top, prev_pizza, prev_not_pizza) =
     if (getI(Q_top)+1 >= 0) andalso not_X(Q_top, (space "data.txt"), 4)
     then
	 if get_bool(is_in_prev(nextD(Q_top), Q_top, #"D", prev_pizza, prev_not_pizza))
	 then (Q, prev_pizza, prev_not_pizza) 
	 else (nextD(Q_top)::Q, prev_pizza, prev_not_pizza)   
     else  (Q, prev_pizza, prev_not_pizza)   
      

fun w_move (Q, Q_top, inpArray, prev_pizza, prev_not_pizza) =
     if Char.compare(Array2.sub(inpArray, getI(Q_top), getJ(Q_top)), #"W")=EQUAL
     then
	 if get_bool(is_in_prev(nextW(Q_top), Q_top, #"W", prev_pizza, prev_not_pizza)) 
	 then (Q, prev_pizza, prev_not_pizza) 
	 else (nextW(Q_top)::Q, prev_pizza, prev_not_pizza)  
     else (Q, prev_pizza, prev_not_pizza)    
      


fun built_path ([], pizza, i, j, prev_pizza, prev_not_pizza) = []
  | built_path (myList, pizza, i, j, prev_pizza, prev_not_pizza) =
    if Char.compare (hd myList, #"O")=EQUAL then tl myList
    else
	if (pizza) then
	    let
		val previous = Array2.sub(prev_pizza,i,j)
	    in
		built_path( (get_move(previous))::myList,
			    get_prev_pizza (previous),
			    get_prev_i(previous),
			    get_prev_j(previous),
			    prev_pizza,
			    prev_not_pizza )
	    end
	else
	    let
		val previous = Array2.sub(prev_not_pizza,i,j)
	    in
		built_path( (get_move(previous))::myList,
			    get_prev_pizza (previous),
			    get_prev_i(previous),
			    get_prev_j(previous),
			    prev_pizza,
			    prev_not_pizza )
	    end  
	


      
(*returns a tuple (cost,path)*)
fun path (iE, jE, Q_top, prev_pizza, prev_not_pizza) =
  if Char.compare (getI(Q_top), iE) = EQUAL andalso
     Char.compare (getJ(Q_top), jE) = EQUAL andalso
     getPizza(Q_top) then
      let
	  val cost = getCost (Q_top)
	  val previous = Array2.sub(prev_pizza,iE,jE)
	  val move = get_move (previous)
      in
	  (cost,
	   built_path([move],
		      get_prev_pizza(previous),
		      get_prev_i(previous),
		      get_prev_j(previous),
		      prev_pizza,
		      prev_not_pizza))
      end
  else
      (0,[])



	      

	

fun main ([], inpArray, prev_pizza, prev_not_pizza) = (0,[])
  | main (Q,  inpArray, prev_pizza, prev_not_pizza) =
    let
	val Qs = sort_Q(Q)
	val Q_top = hd Qs
        val endE = find_end (inpArray, 0, 0)
	val iE = find_i (endE)
	val jE = find_j (endE)
	val _ = path (iE, jE, Q_top, prev_pizza, prev_not_pizza)
        val (Q1,prev1,prev_not1) = l_move (tl Qs, Q_top, prev_pizza, prev_not_pizza)
	val (Q2,prev2,prev_not2) = r_move (Q1, Q_top, prev1, prev_not1)
	val (Q3,prev3,prev_not3) = u_move (Q2, Q_top, prev2, prev_not2)
	val (Q4,prev4,prev_not4) = d_move (Q3, Q_top, prev3, prev_not3)
	val (Q5,prev5,prev_not5) = w_move (Q4, Q_top, inpArray, prev4, prev_not4)
    in
	main (Q5, inpArray, prev5, prev_not5)
    end
	
(*initializes prev's and calls the main function*)	       
fun prev_init (inpArray) =
    let
	val r = Array2.nRows inpArray
	val c = Array2.nCols inpArray
	val prev_pizza = Array2.array (r, c, (~1,~1,#"Z",false))
        val prev_not_pizza = Array2.array (r, c, (~1,~1,#"Z",false))
        val start = find_start (inpArray, 0, 0)
        val iS = find_i (start)
	val jS = find_j (start)
	val _ =   Array2.update(prev_pizza, iS, jS, (~1,~1,#"O",true)) (*init of prev_pizza[start]*)
    in
	main ([(0,true,iS,jS)],  inpArray, prev_pizza, prev_not_pizza)
    end

#include <string>
#include <tuple>
#include <vector>
#include <queue>
#include <list>
#include <fstream>
#include <iostream>
using namespace std;

static char inpData[1000][1000];
static tuple<int,int,char> previ[2][1000][1000];


int main (int argc, char **argv) {
	ifstream myfile;
	char c;
	string line;
	int M=0, N=0, iS=-1, jS=-1, iE=-1, jE=-1;
	int min_cost=0;
	//static char **inpData=0;

	myfile.open(argv[1]);
	while (1) {
		myfile.get(c);
		if (c == '\n') break;
		M++;
	}
	N++;		//just read the first line
	while (getline(myfile, line)) N++;
	
	myfile.clear();				//goes to the beggining of myfile to read input again
	myfile.seekg(0, ios::beg);
	
	//creats input array where inpData[i,j]='char in this position'
	//inpData = new char *[1000]; //////////////////////////////////////////////////////////
	//for (int count=0; count < 1000; count++) inpData[count] = new char [1000]; /////////////////////////////////////

	for (int i=0; i<N; i++) {
		for (int j=0; j<=M; j++) {
			myfile.get(c);
			if (c == '\n') continue;
			inpData[i][j] = c;
			if (c == 'S') { iS=i; jS=j;}
			if (c == 'E') { iE=i; jE=j;}
		}
	}


	class state {
	public:
		bool pizza;
		int i;
		int j;
	}; 

	class Q_node {
	public:
		bool pizza;
		int i;
		int j;
		int cost;
	};

	class mycomparison {
	public:
	bool operator() (const Q_node& lhs, const Q_node& rhs) const {
		return (lhs.cost>rhs.cost);
		}
	};

	typedef priority_queue<int,vector<Q_node>,mycomparison> myPrQ;
	myPrQ Q;
	
	state nextL, nextR, nextU, nextD, nextW,  m;
	Q_node q0, m0, m1;
	int c0;
	list <char> path;

	//static tuple<int,int,char> ***previ=0;		//previ[2][N][M]
	//previ = new tuple<int,int,char> **[2];
	//for (int count=0; count < N; count++) { 
	//	previ[count] = new tuple<int,int,char> *[N];
	//	for (int count0=0; count0 < M; count0++) 
	//		previ[count][count0] = new tuple<int,int,char> [M];
	//}

	for (int l=0; l<2; l++) {	//initialization of array in char field with 'Z'. if get<2>(previ[pizza,i,j]) = 'Z' ((i,j,pizza)state is not in previ), do stuf
		for (int k=0; k<N; k++) {
			for (int z=0; z<M; z++) {
				get<2>(previ[l][k][z]) = 'Z'; //edw moy xtypaei oti paw na grapsw se apagoreymenes perioxes
			}
		}
	}

	 //state start in queue
	q0.pizza = true;
	q0.i = iS;
	q0.j = jS;
	q0.cost = 0;

	Q.push(q0);
	
	tuple<int,int,char> previS (iS, jS, 'O');
	tuple<int,int,char> previBuffer (-1, -1, 'Z');
	previ[true][iS][jS] = previS;

	while (!Q.empty()) {
		m0 = Q.top();
		m.i = m0.i;
		m.j = m0.j;
		m.pizza = m0.pizza;
		Q.pop();
		if (m.i==iE && m.j==jE && m.pizza){		//if end is found
			min_cost = m0.cost;
			//find path:
			bool piz = true;
			int x=iE, y=jE;
			char letter='E';    
			letter = get<2>(previ[piz][x][y]);
			while (letter != 'O') {
				path.push_front (letter);
				previBuffer = previ[piz][x][y];
				x = get<0>(previBuffer); //=previ_i
				y = get<1>(previBuffer); //=previ_j
				if (letter == 'W') piz = !piz; //=previ_pizza
				else piz = piz;
				letter = get<2>(previ[piz][x][y]); //=m
			}
		}
		else {									//if i 'm in array and next move isn' t X:
			//left
			if ((m.j)-1 >= 0  &&  inpData[m.i][(m.j)-1] != 'X')	{ 
				
				nextL = m;    //creat new state
				nextL.j = (m.j)-1; 
				if (get<2>(previ[nextL.pizza][nextL.i][nextL.j]) == 'Z') {   //if it is not already in previ, put it in and push new state in Q
					tuple<int,int,char> previL (m.i, m.j, 'L');	
					previ[nextL.pizza][nextL.i][nextL.j] = previL;  //previ[state]=previ[pizza,i,j]=(i,j,m)
					if (nextL.pizza) c0=2;
					else c0=1;
					m1.cost = m0.cost + c0;
					m1.i = nextL.i;
					m1.j = nextL.j;
					m1.pizza = nextL.pizza;
					Q.push (m1);
				}
			}
			//right
			if ((m.j)+1 <= M-1  &&  inpData[m.i][(m.j)+1] != 'X') { 
				nextR = m;  
				nextR.j = (m.j)+1; 
				if (get<2>(previ[nextR.pizza][nextR.i][nextR.j]) == 'Z') { 
					tuple<int,int,char> previR (m.i, m.j, 'R');	
					previ[nextR.pizza][nextR.i][nextR.j] = previR;
					if (nextR.pizza) c0=2;
					else c0=1;
					m1.cost = m0.cost + c0;
					m1.i = nextR.i;
					m1.j = nextR.j;
					m1.pizza = nextR.pizza;
					Q.push (m1);
				}
			}
			//up
			if ((m.i)-1 >= 0  &&  inpData[(m.i)-1][m.j] != 'X')	{ 
				nextU = m;  
				nextU.i = (m.i)-1; 
				if (get<2>(previ[nextU.pizza][nextU.i][nextU.j]) == 'Z') {  
					tuple<int,int,char> previU (m.i, m.j, 'U');	
					previ[nextU.pizza][nextU.i][nextU.j] = previU;
					if (nextU.pizza) c0=2;
					else c0=1;
					m1.cost = m0.cost + c0;
					m1.i = nextU.i;
					m1.j = nextU.j;
					m1.pizza = nextU.pizza;
					Q.push (m1);
				}
			}
			//down
			if ((m.i)+1 <= N-1  &&  inpData[(m.i)+1][m.j] != 'X') { 
				nextD = m;  
				nextD.i = (m.i)+1; 
				if (get<2>(previ[nextD.pizza][nextD.i][nextD.j]) == 'Z') { 
					tuple<int,int,char> previD (m.i, m.j, 'D');	
					previ[nextD.pizza][nextD.i][nextD.j] = previD;
					if (nextD.pizza) c0=2;
					else c0=1;
					m1.cost = m0.cost + c0;
					m1.i = nextD.i;
					m1.j = nextD.j;
					m1.pizza = nextD.pizza;
					Q.push (m1);
				}
			}
			//if there is wormhole, staying there and taking or leaving pizza is an option:
			if (inpData[m.i][m.j] == 'W') { 
				if (get<2>(previ[m.pizza][m.i][m.j]) != 'W') {   //if last move wasn't 'W'
					nextW = m; 
					nextW.pizza = !(m.pizza); 
					tuple<int,int,char> previW (m.i, m.j, 'W');	
					previ[nextW.pizza][nextW.i][nextW.j] = previW;
					c0=1;
					m1.cost = m0.cost + c0;
					m1.i = nextW.i;
					m1.j = nextW.j;
					m1.pizza = nextW.pizza;
					Q.push (m1);
				}
			}
		}
	}
	cout << endl << min_cost << " ";
	for (list<char>::iterator it = path.begin() ; it != path.end(); ++it)
				cout << *it;
	
	myfile.close();
	return 0;
}
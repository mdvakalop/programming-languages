//My program was affected by the algorithm about disjoint-set-data written in the following link:
//http://www.geeksforgeeks.org/disjoint-set-data-structures-java-implementation/

import java.io.*;
import java.util.*;

public class Villages {

	public static void main(String[] args) {
		
		try {
			String line="";
			String [] nums_str;
			int c, N, M, K, vil1, vil2;
			BufferedReader in =
		    		new BufferedReader (new FileReader (args[0]));
			line = in.readLine();
			nums_str = line.split(" "); //num_str : array of strings
			N = Integer.parseInt(nums_str[0]);
			M = Integer.parseInt(nums_str[1]);
			K = Integer.parseInt(nums_str[2]);
			//System.out.println (N+" "+M+" "+K);
			
			Set villages = new Set (N);
			for (int i=0; i<M; i++) {
				String l = in.readLine();
				String [] nums = l.split(" ");
				vil1 = Integer.parseInt(nums[0]);
				vil2 = Integer.parseInt(nums[1]);
				//System.out.println (vil1+" "+vil2);
				villages.union(vil1-1, vil2-1);
			} //after this procedure we have initialize our sets of villages
			in.close();
			if (villages.get_N() <= K) System.out.println(1);
			else System.out.println(villages.get_N() - K); 
		}
		catch (Exception e) {
			System.out.println ("exception thrown: " + e);
		}
	}
}
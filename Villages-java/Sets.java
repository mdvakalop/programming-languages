import java.io.*;
import java.util.*;
 
class Set
{
    private int [] rank;
    private int [] parent;
    private int N;
    
    //constructor, N0: number of elements, creates rank array = 0, parent(i) = i
    public Set (int N0) {
    	N=N0;
        rank = new int[N0]; 
        parent = new int[N0];
        N = N0;
        create_Set();
    }
    
    //returns rank[n]
    public int get_N () {
    	return this.N; 
    	}
    
    // Initialize sets (firstly, each set contains only its root)
    public void create_Set() {
        for (int i=0; i<N; i++) {
        	parent[i] = i;
        	rank[i] = 0;
        }
    }
    
    //takes x and returns the head-label of the set to which x belongs
    public int find (int x) {
    	while (parent[x] != x) 
    		x = find(parent [x]);
    	return x;
    }
    
    //returns if x,y belong to the same set OR
    // creates new union-set if x,y don't.
    public void union (int x, int y) {
    	int xRoot = this.find (x);
    	int yRoot = this.find (y);
    	if (xRoot == yRoot) return;
    	
    	if (rank[xRoot] > rank[yRoot]) {
    		parent[yRoot] = xRoot;
    		this.N--;
    	}
    	if (rank[xRoot] < rank[yRoot]) {
    		parent[xRoot] = yRoot;
    		this.N --;
    	}
    	if (rank[xRoot] == rank[yRoot]) {
    		parent[yRoot] = xRoot;  //doesn't matter x or y
    		rank[xRoot]++;
    		this.N --;
    	}
	}
}
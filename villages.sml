fun get_v1 (v1,v2) = v1
fun get_v2 (v1,v2) = v2


fun find (parents, x) =
  let
      val par_x = Array.sub (parents, x)
  in
      if Int.compare (par_x, x) = EQUAL then x
      else find (parents, par_x)
  end
      
			  




(*returns updated arrays ranks & parents and n*)
fun about_ranks (parents, ranks, root1, root2, n) =
  if Int.compare (root1, root2) = EQUAL then (parents, ranks, n)
  else
     if Int.compare(Array.sub (ranks, root1), Array.sub (ranks, root2))=LESS
     then
	 let
	     val _ = Array.update (parents, root1, root2)
	 in
	     (parents, ranks, n-1)
	 end
     else
	 if Int.compare(Array.sub (ranks, root1),Array.sub (ranks, root2))=GREATER
	 then
	     let
		 val _ = Array.update (parents, root2, root1)
	     in
		 (parents, ranks, n-1)
	     end
	 else (*if equal*)
	     let
		 val _ = Array.update (parents, root2, root1)
		 val rank1 = Array.sub (ranks, root1)		      
		 val _ = Array.update (ranks, root1, rank1+1)
	     in
		 (parents, ranks, n-1)
	     end
	     
			 
fun union (parents, ranks, [], n) = n
  | union (parents, ranks, pairs_list, n) =
    let
	val pair = hd pairs_list
	(*val root1 = Array.sub (parents, get_v1(pair))
	val root2 = Array.sub (parents, get_v2(pair))*)
	val root1 = find (parents, get_v1(pair)-1)
	val root2 = find (parents, get_v2(pair)-1)	      
	val (up_parents, up_ranks, up_n)
	    = about_ranks (parents, ranks, root1, root2, n)
    in
	union (up_parents, up_ranks, tl pairs_list, up_n)
    end
				 

fun compute_result (n, k) =
  if n <= k then 1
  else n-k
	   

fun init_parents (par0, ~1) = par0
  | init_parents (par0, n) =
  let
      val _ = Array.update (par0, n, n)
  in
      init_parents (par0, n-1)
  end
	     

(*returns int. union type things returns integer*)
fun init (n, k, [])         = n-k
  | init (n, k, pairs_list) =
    let
	val parents0 = Array.array (n, 0) (*for start,parent of n is n*)
	val parents = init_parents (parents0, n-1)
	val ranks = Array.array (n, 0) (*ranks=0*)
    in
	compute_result(union (parents, ranks, pairs_list, n),k) (*check n,m,k*)
    end  


(*reads data from file. returns integer)*)
fun villages file =
  let
    fun next_int input =
	    Option.valOf (TextIO.scanStream (Int.scan StringCvt.DEC) input)
    val stream = TextIO.openIn file
    val N = next_int stream
    val M = next_int stream
    val K = next_int stream
  in
      if N <= K then 1
      else
	  let 
	      fun scanFile 0 acc = acc (* acc is my input list reversed*)
		| scanFile i acc =
		  let
                      val v1 = next_int stream
		      val v2 = next_int stream
		  in
                      scanFile (i - 1) ((v1,v2) :: acc)
		  end
	  in
	      init(N, K, scanFile M [])
	  end
  end
      

import java.util.Comparator;

public class My_comparator implements Comparator<Q_node>{

	@Override
	public int compare(Q_node q0, Q_node q1) {
		return q0.get_cost() - q1.get_cost();		
	}
	
}

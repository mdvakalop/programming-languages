import java.io.*;
import java.util.Comparator;
import java.util.PriorityQueue;

public class Moredeli {

	public static void main(String[] args) {
		try {
		    BufferedReader in0 =
		    		new BufferedReader (new FileReader (args[0]));
		    char [][] inpData = new char [1000][1000];
		    char c;
		    
		    //find N,M
		    String line;
		    int rows=0, columns=0, iS, jS, iE, jE;
		    iS=jS=iE=jE=-1;
		    line = in0.readLine(); //line doesn't contain terminator \n or \r
		    columns = line.length();
		    while (line!=null){
			rows++;
			line = in0.readLine();
		    }
		    in0.close();
		    
		  //inpData
		    BufferedReader in =
					new BufferedReader (new FileReader (args[0]));
		    for(int i=0; i<rows; i++) {
		    	for(int j=0; j<columns; j++) {
		    		c = (char)in.read();
		    		inpData[i][j] = c;
		    		if (c == 'S') { iS=i; jS=j;}
					if (c == 'E') { iE=i; jE=j;}
		    	}
		    	c = (char)in.read();   //\n
		    	//c = (char)in.read();   //\r
		    }
		    in.close();
		
		    //priority queue
		    Comparator<Q_node> my_comp = new My_comparator ();
		    PriorityQueue<Q_node> Q = 
		    		new PriorityQueue<Q_node>(1,my_comp);
		    
		    Q_node next, m0, q0;
		    String path = "";
		    int min_cost = 1;  //just an initialization
		    m0 = new Q_node (0,0,0);
		    
		    //previ
		    Tuple [][] previ = new Tuple [1000][1000]; //prev(state.i,state.j) = (iprev,jprev,cost'move')
		    for (int k=0; k<rows; k++) //initialization of array in char field with 'Z'. if  'Z' state is not in previ, do stuf
		    	for (int z=0; z<columns; z++) 
		    		previ[k][z] = new Tuple (k, z, 0, 'Z');
		    
		    //start state in queue
		    q0 = new Q_node (iS, jS, 0);
		    Q.add(q0);
		    //previ of start-state
		    Tuple previS = new Tuple(iS, jS, 0, 'O');
		    Tuple previBuffer = new Tuple (-1, -1, 1, 'Z');
		    previ[iS][jS] = previS;
		    
		    while (Q.size() > 0) {   // Q.size() > 1
		    	m0 = Q.poll();  //poll gives m0 head and removes it
		    	int mi = m0.get_i();
		    	int mj = m0.get_j();
		    	int mc = m0.get_cost();
		    	
		    	if (mi == iE && mj == jE){		//if end is found
					min_cost = mc;
					//find path:
					int x=iE, y=jE;
					char letter='E';    
					letter = (char)previ[x][y].get_move();
					while (letter != 'O') {
						path = letter + path;
						previBuffer = previ[x][y];
						x = previBuffer.get_i(); //=previ_i
						y = previBuffer.get_j(); //=previ_j
						letter = (char)previ[x][y].get_move(); //=m
					}
				}
				else {									//if i 'm in array and next move isn' t X:
					//left costs 2' 
					if (mj-1 >= 0  &&  inpData[mi][(mj)-1] != 'X')	{ 
						next = new Q_node (mi, mj-1,mc+2);
						int ii = next.get_i();
						int jj = next.get_j();
						int cc = next.get_cost();
						if ((char)previ[ii][jj].get_move() == 'Z' || cc < previ[ii][jj].get_cost()) {   
							//if it is not already in previ, or you arrived there with less cost, put it in and push new state in Q
							Tuple previNext = new Tuple (mi, mj, cc, 'L');	
							previ[ii][jj] = previNext;  //previ[state]=previ[pizza,i,j]=(i,j,m) 
							Q.add (next);
						}
					}
					//right costs 1' 
					if (mj+1 < columns  &&  inpData[mi][(mj)+1] != 'X')	{ 
						next = new Q_node (mi, mj+1, mc+1);
						int ii = next.get_i();
						int jj = next.get_j();
						int cc = next.get_cost();
						if ((char)previ[ii][jj].get_move() == 'Z' || cc < previ[ii][jj].get_cost()) {   
							//if it is not already in previ, or you arrived there with less cost, put it in and push new state in Q
							Tuple previNext = new Tuple (mi, mj, cc, 'R');	
							previ[ii][jj] = previNext;  //previ[state]=previ[pizza,i,j]=(i,j,m) 
							Q.add (next);
						}
					}
					//up costs 3' 
					if (mi-1 >= 0  &&  inpData[mi-1][mj] != 'X')	{ 
						next = new Q_node (mi-1, mj, mc+3);
						int ii = next.get_i();
						int jj = next.get_j();
						int cc = next.get_cost();
						if ((char)previ[ii][jj].get_move() == 'Z' || cc < previ[ii][jj].get_cost()) {   
							//if it is not already in previ, or you arrived there with less cost, put it in and push new state in Q
							Tuple previNext = new Tuple (mi, mj, cc, 'U');	
							previ[ii][jj] = previNext;  //previ[state]=previ[pizza,i,j]=(i,j,m) 
							Q.add (next);
						}
					}
					//down costs 1' 
					if (mi+1 < rows  &&  inpData[mi+1][mj] != 'X')	{ 
						next = new Q_node (mi+1, mj, mc+1);
						int ii = next.get_i(); 
						int jj = next.get_j();
						int cc = next.get_cost();
						if ((char)previ[ii][jj].get_move() == 'Z' || cc < previ[ii][jj].get_cost()) {   
							//if it is not already in previ, or you arrived there with less cost, put it in and push new state in Q
							Tuple previNext = new Tuple (mi, mj, cc, 'D');	
							previ[ii][jj] = previNext;  //previ[state]=previ[pizza,i,j]=(i,j,m) 
							Q.add (next);
						}
					}
				}
		    }
		    System.out.print(min_cost+" "+path);
		}
		catch (Exception e) {
			System.out.println ("exception thrown: " + e);
		}
	}
}


public class Tuple extends Q_node{
	private char move;
	
	public Tuple(int i0, int j0, int cost0, char move0) {
		super (i0,j0,cost0);
		move = move0;
	}
	
	public int get_move () { return move; }
	public void set_move (char move0) { move = move0; }
	
}

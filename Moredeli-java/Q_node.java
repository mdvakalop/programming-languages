public class Q_node {
	protected int i;
	protected int j;
	protected int cost;
	
	public Q_node (int i0, int j0,  int cost0) {
		this.i = i0;
		this.j = j0;
		this.cost = cost0;
	}
	
	public int get_i () { return i; }
	public int get_j () { return j; }
	public int get_cost () { return cost; }
	
	public void set_i (int i0) { i = i0; }
	public void set_j (int j0) { j = j0; }
	public void set_cost (int cost0) { cost = cost0; }
}

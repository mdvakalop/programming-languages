#include <iostream>
#include <vector>
#include <fstream>
using namespace std;

//stairs begin from stair-0.

int main (int argc,char *argv[]) {
	int N,K,B;
	int c, next_stair;
	unsigned long int ways=0;
	
	ifstream myfile;
	myfile.open (argv[1]);
	myfile >> N;
	myfile >> K;
	myfile >> B;

	//initializing vectors
	vector <int> Steps, Stairs(N,0); // Stairs[i]=-1 <=> the i-th stair is broken, else Stairs[i]=k <=> there are k ways to reach i-th stair

	for (int i=0; i<K; i++) {
		myfile >> c;
		Steps.push_back(c);
	}
	for (int i=0; i<B; i++) {
		myfile >> c;
		Stairs[c-1] = -1;  //the c-th stair is broken
	}

	if (Stairs[0] != -1 && Stairs[N-1] != -1) {
		Stairs[0] = 1;  //one way to reach the first stair
		for (int j=0; j<N; j++) {  // j is the current stair
			if (Stairs[j] != 0 && Stairs[j] != -1) //if stair is reachable and not broken
				for (int i=0; i<K; i++) {
					next_stair = j+Steps[i];
					if (next_stair < N && Stairs[next_stair] != -1) {
						Stairs[next_stair] += Stairs[j];
						Stairs[next_stair] = Stairs[next_stair] % 1000000009;
					}
				}
		}
	}
	ways = Stairs[N-1];
	cout << ways << endl;

	myfile.close();
	return 0;
}
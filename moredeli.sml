(*function taken from lecture-6.pdf of pl1*)
fun reverse (xs) =  
  let
      fun rev (nil, z)   = z
	| rev (y::ys, z) = rev (ys, y::z)
  in
      rev (xs, nil)
  end


(*this fun returns line_list without "\n"*)
fun char_list ([]) = []
  | char_list (l)  = reverse(tl (reverse(l)))



(*getters*)
fun get_cost_Q_node  (i,j,cost)  =  cost			  
fun get_i_Q_node     (i,j,cost)  =  i
fun get_j_Q_node     (i,j,cost)  =  j

fun get_move (i,j,move,cost) = move
fun get_prev_i (i,j,move,cost) = i
fun get_prev_j (i,j,move,cost) = j
fun get_prev_cost (i,j,move,cost) = cost					

fun get_bool (a,b,c) = a (*this getter concerns previ is_in_previ*)	


fun insert a [] = [a]  									
  | insert (i1,j1,cost1) ((i2,j2,cost2):: t) =  
    if cost1 < cost2 then (i1,j1,cost1)::(i2,j2,cost2)::t  
    else (i2,j2,cost2)::insert (i1,j1,cost1) t  



	 

(*funs for fun not_X (1:left, 2:right, 3:up, 4:down)*)				      
fun addI (1) = 0
  | addI (2) = 0
  | addI (3) = ~1
  | addI (4) = 1
  | addI (n) = 0 

fun addJ (1) = ~1
  | addJ (2) = 1
  | addJ (3) = 0
  | addJ (4) = 0
  | addJ (n) = 0

fun not_X (node, inpArray, move) =
  let
      val i0 =  get_i_Q_node(node) + addI(move)
      val j0 =  get_j_Q_node(node) + addJ(move)
  in
      if (Char.compare (Array2.sub(inpArray, i0, j0), #"X") = EQUAL) then false
      else true (*true: yes, this move is valid (there is not X)*)
  end

(* returns true if prev[next_node] is written*)
fun not_in_previ (next_node, previ) = (*Move is a char *)
  let
      val iNext = get_i_Q_node(next_node)
      val jNext = get_j_Q_node(next_node)				
  in
      (*if it isn't in prev*)
      if Char.compare(get_move(Array2.sub (previ, iNext, jNext)), #"Z") = EQUAL
	 orelse
	 Char.compare(get_move(Array2.sub (previ, iNext, jNext)), #"O") = EQUAL
      then true
      else false
  end
     
      

(* takes node as variable and returns a node with the new-left-state*)
fun nextL (i, j, cost) = (i, j-1, cost+2)


(*returns a node with the new-right-state*)
fun nextR (i, j, cost) = (i, j+1, cost+1)

(*returns a node with the new-up-state*)
fun nextU (i, j, cost) = (i-1, j, cost+3)


(*returns a node with the new-down-state*)
fun nextD (i, j, cost) = (i+1, j, cost+1)
 

(*all moves work*)
(* k_move (tl Q, Q_top, previ,inpArray): returns updated (Q,previ) *) 			   
fun l_move (Q, Q_top, previ, inpArray) =
     if get_j_Q_node(Q_top)-1 >= 0 andalso not_X(Q_top, inpArray, 1)
     then
	 let		  
	     val next = nextL(Q_top) (* next = (i,j,cost)*)
	     val cc = get_cost_Q_node (next)
	     val ii = get_i_Q_node(next)
	     val jj = get_j_Q_node(next)
	     val prev_cost = get_prev_cost( Array2.sub (previ, ii, jj) )
	 in
	     if not_in_previ(next,previ) (*if it isnt in privi*)
		orelse cc < prev_cost then
		 let
		     val _ = Array2.update(previ, ii, jj,
			     (get_i_Q_node(Q_top),get_j_Q_node(Q_top), #"L",cc))
		 in
		     (insert next Q , previ)
		 end
	     else (Q, previ)
	 end
     else (Q, previ)
	      

fun r_move (Q, Q_top, previ, inpArray) =
  if get_j_Q_node(Q_top)+1 < Array2.nCols inpArray
     andalso not_X(Q_top, inpArray, 2)
     then
	 let		  
	     val next = nextR(Q_top) (* next = (i,j,cost)*)
	     val cc = get_cost_Q_node (next)
	     val ii = get_i_Q_node(next)
	     val jj = get_j_Q_node(next)
	     val prev_cost = get_prev_cost( Array2.sub (previ, ii, jj) )
	 in
	     if not_in_previ(next,previ) (*if it isnt in privi*)
		orelse cc < prev_cost then
		 let
		     val _ = Array2.update(previ, ii, jj,
			     (get_i_Q_node(Q_top),get_j_Q_node(Q_top), #"R",cc))
		 in
		     (insert next Q , previ)
			
		 end
	     else (Q, previ)
	 end
     else (Q, previ)		      


fun u_move (Q, Q_top, previ, inpArray) =
  if get_i_Q_node(Q_top)-1 >= 0 andalso not_X(Q_top, inpArray, 3)
     then
	 let		  
	     val next = nextU(Q_top) (* next = (i,j,cost)*)
	     val cc = get_cost_Q_node (next)
	     val ii = get_i_Q_node(next)
	     val jj = get_j_Q_node(next)
	     val prev_cost = get_prev_cost( Array2.sub (previ, ii, jj) )
	 in
	     if not_in_previ(next,previ) (*if it isnt in privi*)
		orelse cc < prev_cost then
		 let
		     val _ = Array2.update(previ, ii, jj,
			     (get_i_Q_node(Q_top),get_j_Q_node(Q_top), #"U",cc))
		 in
		     (insert next Q , previ)
		 end
	     else (Q, previ)
	 end
     else (Q, previ)


fun d_move (Q, Q_top, previ, inpArray) =
  if get_i_Q_node(Q_top)+1 <  Array2.nRows inpArray
     andalso not_X(Q_top, inpArray, 4)
     then
	 let		  
	     val next = nextD(Q_top) (* next = (i,j,cost)*)
	     val cc = get_cost_Q_node (next)
	     val ii = get_i_Q_node(next)
	     val jj = get_j_Q_node(next)
	     val prev_cost = get_prev_cost( Array2.sub (previ, ii, jj) )
	 in
	     if not_in_previ(next,previ) (*if it isnt in privi*)
		orelse cc < prev_cost then
		 let
		     val _ = Array2.update(previ, ii, jj,
			     (get_i_Q_node(Q_top),get_j_Q_node(Q_top), #"D",cc))
		 in
		     (insert next Q , previ)(*pushed in Q*)
		 end
	     else (Q, previ)
	 end
     else (Q, previ)
	      



(*this fun returns (inpArray, iStart, jStart). take find_start for 1st time with iSt=jSt=0*)
fun find_start (array, i, j) =
  if  ((Array2.nCols array)-1 < j) orelse ((Array2.nRows array)-1 < i)
  then (array, ~1, ~1) (*out of array bound, start hasn't got found*)
  else
      if (Char.compare(Array2.sub(array,i,j),#"S") = EQUAL) then (array, i, j)
      else
	  if ((Array2.nCols array)-1 = j) then find_start (array, i+1, 0)
	  else find_start (array, i, j+1)

fun find_end (array, i, j) =
  if  ((Array2.nCols array)-1 < j) orelse ((Array2.nRows array)-1 < i)
  then (array, ~1,~1)
  else
      if (Char.compare(Array2.sub(array,i,j), #"E") = EQUAL) then (array, i, j)
      else
	  if ((Array2.nCols array)-1 = j) then find_end (array, i+1, 0)
	  else find_end (array, i, j+1)

fun find_i (array, i, j) = i
fun find_j (array, i, j) = j	  






	      

(*returns char list of moves*)
fun built_path (iS, jS, [], i, j, previ) = " "
  | built_path (iS, jS, myList, i, j, previ) =
    (*if Char.compare (hd myList, #"O")=EQUAL then tl myList*)
    if Int.compare (iS,i)=EQUAL andalso Int.compare (jS,j)=EQUAL
    then String.concat myList
    else
	let
	    val previous = Array2.sub(previ,i,j)
	in
	    built_path( iS, jS,
			(Char.toString(get_move(previous)))::myList,
			get_prev_i(previous),
			get_prev_j(previous),
			previ )
	end 


(*cost works*)	       
(*returns a tuple (cost,path). fun finds cost and calls built_path for path*)
fun cost_path (iS, jS, iE, jE, Q_top, previ) =
  let
      val cost = get_cost_Q_node (Q_top)
      val previous = Array2.sub(previ,iE,jE)
  in
      (cost, built_path(iS, jS,
			[Char.toString(get_move (previous))],
			get_prev_i(previous),
			get_prev_j(previous),
			previ))
  end


      
(*takes q0 fisrt and begins "while" procedure*)
fun main (iS, jS, [], inpArray, previ) = (0," ")
  | main (iS, jS, Q,  inpArray, previ) =
    let
	val Q_top = hd Q
        val endE = find_end (inpArray, 0, 0)
	val iE = find_i (endE) (*these work, it finds end*)
	val jE = find_j (endE)
    in
	if Int.compare (get_i_Q_node(Q_top), iE) = EQUAL andalso
           Int.compare (get_j_Q_node(Q_top), jE) = EQUAL then
	    (*(get_cost_Q_node (Q_top), [get_move(Array2.sub(previ,iE,jE))])*)
	     cost_path (iS, jS, iE, jE, Q_top, previ) (*returns (cost,path)*)
	else
	    let    (*k_move returns the updated : (Q priority,previ)*) 
	        val (Q1,prev1) = l_move (tl Q, Q_top, previ, inpArray)
		val (Q2,prev2) = r_move (Q1, Q_top, prev1, inpArray)
		val (Q3,prev3) = u_move (Q2, Q_top, prev2, inpArray)
		val (Q4,prev4) = d_move (Q3, Q_top, prev3, inpArray)
	(* Q gets added with nodes from l_move,r_move,... 
        diadoxika and ends in Q4 after all adds*)
	    in
		main (iS, jS, Q4, inpArray, prev4)
	    end
   end




			       
(*works*)
(*initializes prev and calls the main function*)	       
fun solver (inpArray) =
    let
	val N = Array2.nRows inpArray
	val M = Array2.nCols inpArray
	val previ = Array2.array (N, M, (~1,~1,#"Z",0))
	(*previ: NxM array cointaining tuples:(i,j,move,cost)*)
        val start = find_start (inpArray, 0, 0)
        val iS = find_i (start)
	val jS = find_j (start)
	val _ = Array2.update(previ, iS, jS, (~1,~1,#"O",0)) (*init of previ[start]*) (*this is unnecessary*)
    in
	main (iS,jS, [(iS,jS,0)] , inpArray, previ ) (*Q_node: (i,j,cost)*)
    end
	

	
(*works*)
fun moredeli file =
  let
      val stream = TextIO.openIn file
      fun next_line input = (TextIO.inputLine input)
  in
      let
	  fun built_list (l) =
	    if (TextIO.endOfStream stream) then l (*l: list of lists*)
	    else
		let
		    val a = next_line stream
		    val b = explode (Option.valOf(a))
		    val s = char_list (b)
		in
		    built_list (s::l)
		end
      in
	  solver(Array2.fromList (reverse(built_list ([]))))
       end
 end
     
